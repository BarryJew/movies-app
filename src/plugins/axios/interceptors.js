function returnData(res) {
  return res.data;
}

export default function (axios) {
  // axios.interceptors.request.use(setParams); //// если нужно на запрос
  axios.interceptors.response.use(returnData);
}
